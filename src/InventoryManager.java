import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class InventoryManager {
    private final String FILE_NAME;
    private List<InventoryItem> items;

    public List<InventoryItem> getItems() {
        return items;
    }

    public InventoryManager(String path) {
        items = new ArrayList<>();
        FILE_NAME = path;
    }

    // considering that files are written with comma
    public List<InventoryItem> readInventoryItems() throws FileIOException {
        List<String> fileContent;
        Path path = Paths.get(FILE_NAME);

        try {
            fileContent = Files.readAllLines(path);
        } catch (IOException e) {
            throw new FileIOException("Failed to read inventory items from file located in: " + path);
        }

        for (String line : fileContent) {
            String[] fields = line.split(",");

            InventoryItem newItem = new InventoryItem(
                    Integer.parseInt(fields[0].trim()),
                    fields[1].trim(),
                    Integer.parseInt(fields[2].trim()),
                    Double.parseDouble(fields[3].trim())
            );

            items.add(newItem);
        }

        return items;
    }

    public void writeInventoryItem(List<InventoryItem> items) throws FileIOException {
        Path path = Paths.get(FILE_NAME);

        try {
            Files.write(path, items.toString().getBytes());
        } catch (IOException e) {
            throw new FileIOException("Failed to write to inventory items from file located in: " + path);
        }
    }

    public void addInventoryItem(InventoryItem item) throws InvalidDataException {
        if (item.getQuantity() >= 0 && item.getPrice() >= 0
                && item.getName() != null && items.stream().anyMatch(i -> i.getItemID() != item.getItemID())) {
            items.add(item);
        } else {
            throw new InvalidDataException("Quantity, price can't be negative, " +
                    "the name should not be null, and the IDs are unique.");
        }
    }

    public InventoryItem findItem(int itemID) throws ItemNotFoundException {
        InventoryItem result = items.stream()
                .filter(i -> i.getItemID() == itemID)
                .findAny()
                .orElse(null);

        if (result == null) {
            throw new ItemNotFoundException("Item does not exist.");
        } else {
            return result;
        }
    }

    public void updateItem(InventoryItem currentItem, InventoryItem newItem) throws InvalidDataException {
        if (newItem.getItemID() >= 0 && newItem.getQuantity() >= 0
        && newItem.getPrice() >= 0 && newItem.getName() != null) {
            items.stream()
                    .filter(i -> i.getItemID() == currentItem.getItemID())
                    .forEach(i -> {
                        i.setName(newItem.getName());
                        i.setPrice(newItem.getPrice());
                        i.setQuantity(newItem.getQuantity());
                    });
        } else {
            throw new InvalidDataException("The properties of new added item are not accepted.");
        }
    }

    public void deleteItem(InventoryItem item) throws ItemNotFoundException {
        if (this.findItem(item.getItemID()) != null) {
            items.removeIf(i -> i.getItemID() == item.getItemID());
        } else {
            throw new ItemNotFoundException("Item does not exist.");
        }
    }
}