import java.util.List;

public class Main {
    public static void main(String[] args) throws FileIOException, InvalidDataException, ItemNotFoundException {
        InventoryManager inputManager = new InventoryManager("./src/files/input.txt");
        List<InventoryItem> items = inputManager.readInventoryItems();
        for (InventoryItem item : items) {
            System.out.println(item.toString());
        }
        System.out.println("====================================");

        inputManager.addInventoryItem(new InventoryItem(5, "product6", 1, 20));
//        inputManager.addInventoryItem(new InventoryItem(0, "product3", 0, 0)); // should give exception
        System.out.println("After add:");
        inputManager.getItems().forEach(i -> System.out.println("\t" + i.toString()));
        System.out.println("====================================");

        System.out.println("Found item: " + inputManager.findItem(2));
        System.out.println("====================================");

        inputManager.updateItem(inputManager.findItem(2),
                new InventoryItem(2, "changedProduct", 5, 10.2));
        System.out.println("After update:");
        inputManager.getItems().forEach(i -> System.out.println("\t" + i.toString()));
        System.out.println("====================================");

        inputManager.deleteItem(new InventoryItem(5, "product6", 1, 20));
        System.out.println("After delete:");
        inputManager.getItems().forEach(i -> System.out.println("\t" + i.toString()));
        System.out.println("====================================");

        InventoryManager outputManager = new InventoryManager("./src/files/output.txt");
        outputManager.writeInventoryItem(inputManager.getItems());
    }
}