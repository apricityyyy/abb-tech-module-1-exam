import java.io.IOException;

public class FileIOException extends IOException {
    public FileIOException(String errorMessage) {
        super(errorMessage);
    }
}
